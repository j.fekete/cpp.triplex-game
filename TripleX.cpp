#include <iostream>
#include <ctime>


void PrintIntroduction(int Difficulty){
    std::cout<<"Curren level of security is: "<<Difficulty<<std::endl;
    std::cout<<"\n**You need correct password to enter the temple...\n Enter correct code to bypass security measures**\n\n";
} 

bool PlayGame(int Difficulty){

     PrintIntroduction(Difficulty);
     // Generate Code
     const int CodeB = rand() % Difficulty + Difficulty;
     const int CodeA = rand() % Difficulty + Difficulty;
     const int CodeC = rand() % Difficulty + Difficulty;
             
     const int CodeSum = CodeA + CodeB + CodeC;
     const int CodeProduct = CodeA * CodeB * CodeC;

    // Print CodeSum and CodeProduct to the terminal
     std::cout<<"**There are three numbers in the code \n";
     std::cout<<"**The codes add-up to:"<<CodeSum;
     std::cout<<"\n**The codes multiply to give: "<<CodeProduct<<std::endl;

    // Store player guess
     int GuessA, GuessB, GuessC;
     std::cin >> GuessA >> GuessB >> GuessC;

     int GuessSum = GuessA + GuessB + GuessC;
     int GuessProduct = GuessA * GuessB * GuessC;

    // Check if the players guess is correct
     if (GuessSum == CodeSum && GuessProduct==CodeProduct)
     {
        std::cout<<"\n**Well done Shadow! You've cracked this security level!**\n";
        return true;
     }
     else
     {
         std::cout<<"\n**You entered wrong code! Be careful Shadow! Try again!**\n";
         return false;
     }
    }
     
     int main() 
     {
        srand(time(NULL));// create new random sequence based on time of day

        int LevelDifficulty = 1;
        int const MaxLevelDifficulty = 5;
        std::cout<<"**You are a ninja breaking into ancient temple that has modern security system** \n";

        while (LevelDifficulty <= MaxLevelDifficulty) // Loop game until all levels completed
        {
           bool bLevelComplete = PlayGame(LevelDifficulty);
           std::cin.clear(); // Clears any errors
           std::cin.ignore(); // Discards the buffer

           if (bLevelComplete)
           {
               ++LevelDifficulty;
           }
        }
        std::cout<<"\n______________________________________________________________________________\n";
        std::cout<<"\n**Great work Shadow! You cracked every code! The security has been turned off!\n";
        std::cout<<"\n______________________________________________________________________________\n";
         return 0;
     }
     



